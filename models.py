from sqlalchemy import Column, Integer, VARCHAR, DECIMAL, INT
from base import Base, engine


class BinanceLeaderboardTrades(Base):
    __tablename__ = 'binance_leaderboard_trades'

    id = Column(Integer, primary_key=True)
    uid = Column(VARCHAR(255), nullable=False)
    symbol = Column(VARCHAR(255), nullable=False)
    amount = Column(DECIMAL(65, 30))
    update_timestamp = Column(INT, nullable=False)
    receive_unixts = Column(INT, nullable=False)

    def __init__(self, uid: str, symbol: str, amount: float, update_timestamp: int, receive_unixts: int):
        self.uid = uid
        self.symbol = symbol
        self.amount = amount
        self.update_timestamp = update_timestamp
        self.receive_unixts = receive_unixts


if __name__ == '__main__':
    Base.metadata.create_all(engine)
