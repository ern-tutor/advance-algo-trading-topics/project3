import requests


class TGBot:
    def __init__(self, token: str, channel_id: str):
        self.base_url = 'https://api.telegram.org'
        self._token = token
        self.channel_id = channel_id

    def send_message(self, text: str, disable_notification=False):
        params = {
            'chat_id': self.channel_id,
            'text': text,
            'disable_notification': disable_notification
        }

        res = requests.post(f'{self.base_url}/bot{self._token}/sendMessage', data=params)

        return res
