from datetime import datetime


def get_current_unix_ts():
    unix_ts = datetime.utcnow().timestamp()

    return int(unix_ts)
