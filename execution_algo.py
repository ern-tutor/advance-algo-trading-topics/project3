import time
import random

from pybit.unified_trading import HTTP


def custom_chasing_limit_order(
        http_session: HTTP,
        orderbook_depth=1,
        max_refresh_tries=10,
        max_refresh_order_random_seconds=2,
        **kwargs
):
    order_ids = []
    qty = float(kwargs['qty'])
    category = kwargs['category']
    symbol = kwargs['symbol']

    instrument_info = http_session.get_instruments_info(category=category, symbol=symbol)
    lot_size_filter = instrument_info['result']['list'][0]['lotSizeFilter']
    max_order_qty = float(lot_size_filter['maxOrderQty'])
    min_order_qty = float(lot_size_filter['minOrderQty'])
    qty_step = float(lot_size_filter['qtyStep'])

    qty = float(min(max_order_qty, max(min_order_qty, int(qty/qty_step) * qty_step)))
    kwargs['qty'] = str(qty)

    if kwargs['side'] == 'Buy':
        while qty > 0:
            if max_refresh_tries == 0:
                order = http_session.place_order(orderType='Market', **kwargs)
                order_id = order['result']['orderId']
                order_ids.append(order_id)

                return order_ids

            orderbook = http_session.get_orderbook(category=category, symbol=symbol)
            bid_px = orderbook['result']['b'][orderbook_depth-1][0]
            order = http_session.place_order(orderType='Limit', price=bid_px, **kwargs)
            order_id = order['result']['orderId']

            time.sleep(random.randint(1, max_refresh_order_random_seconds))

            open_orders = http_session.get_open_orders(category=category, symbol=symbol, orderId=order_id)
            order_status = open_orders['result']['list'][0]['orderStatus']

            if order_status == 'Filled':
                order_ids.append(order_id)
                return order_ids
            else:
                order_cancel = http_session.cancel_order(category=category, symbol=symbol, orderId=order_id)
                order_qty = open_orders['result']['list'][0]['qty']
                leaves_qty = open_orders['result']['list'][0]['leavesQty']
                qty -= (float(order_qty) - float(leaves_qty))
                kwargs['qty'] = str(qty)
                order_ids.append(order_id)
                max_refresh_tries -= 1
    else:
        while qty > 0:
            if max_refresh_tries == 0:
                order = http_session.place_order(orderType='Market', **kwargs)
                order_id = order['result']['orderId']
                order_ids.append(order_id)

                return order_ids

            orderbook = http_session.get_orderbook(category=category, symbol=symbol)
            ask_px = orderbook['result']['a'][orderbook_depth - 1][0]
            order = http_session.place_order(orderType='Limit', price=ask_px, **kwargs)
            order_id = order['result']['orderId']

            time.sleep(random.randint(1, max_refresh_order_random_seconds))

            open_orders = http_session.get_open_orders(category=category, symbol=symbol, orderId=order_id)
            order_status = open_orders['result']['list'][0]['orderStatus']

            if order_status == 'Filled':
                order_ids.append(order_id)

                return order_ids
            else:
                order_cancel = http_session.cancel_order(category=category, symbol=symbol, orderId=order_id)
                order_qty = open_orders['result']['list'][0]['qty']
                leaves_qty = open_orders['result']['list'][0]['leavesQty']
                qty -= (float(order_qty) - float(leaves_qty))
                kwargs['qty'] = str(qty)
                order_ids.append(order_id)
                max_refresh_tries -= 1


