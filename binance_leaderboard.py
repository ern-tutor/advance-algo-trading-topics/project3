import requests
import pandas as pd

from leaderboard_enums import BinancePeriodTypes, BinanceStatisticsTypes, BinanceTradeTypes


class BinanceLeaderboard(object):
    def __init__(self):
        self.leaderboard_rank_url = 'https://www.binance.com/bapi/futures/v3/public/future/leaderboard/getLeaderboardRank'
        self.position_status_url = 'https://www.binance.com/bapi/futures/v1/public/future/leaderboard/getPositionStatus'
        self.following_status_url = 'https://www.binance.com/bapi/futures/v2/private/future/leaderboard/getFollowingStatus'
        self.other_performance_url = 'https://www.binance.com/bapi/futures/v2/public/future/leaderboard/getOtherPerformance'
        self.leaderboard_base_info_url = 'https://www.binance.com/bapi/futures/v2/public/future/leaderboard/getOtherLeaderboardBaseInfo'
        self.private_positions_url = 'https://www.binance.com/bapi/futures/v2/private/future/leaderboard/getOtherPosition'
        self.public_positions_url = 'https://www.binance.com/bapi/futures/v1/public/future/leaderboard/getOtherPosition'

    def get_leaderboard_ranks(self,
                              trade_type: BinanceTradeTypes,
                              statistics_type: BinanceStatisticsTypes,
                              period_type: BinancePeriodTypes,
                              is_shared=True,
                              is_trader=False) -> pd.DataFrame:
        param = {
            'tradeType': trade_type.value,
            'statisticsType': statistics_type.value,
            'periodType': period_type.value,
            'isShared': is_shared,
            'isTrader': is_trader
        }

        res = requests.post(self.leaderboard_rank_url, json=param)
        j = res.json()

        return pd.DataFrame(j.get('data'))

    def get_positions(self, encrypted_uid: str, trade_type: BinanceTradeTypes) -> dict:
        param = {
            'encryptedUid': encrypted_uid,
            'tradeType': trade_type.value
        }

        res = requests.post(self.public_positions_url, json=param)
        j = res.json()

        return j.get('data')
