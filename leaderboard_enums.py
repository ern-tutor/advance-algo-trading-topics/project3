from enum import Enum


class BinancePeriodTypes(Enum):
    DAILY = 'DAILY'
    WEEKLY = 'WEEKLY'
    MONTHLY = 'MONTHLY'
    ALL = 'ALL'


class BinanceStatisticsTypes(Enum):
    ROI = 'ROI'
    PNL = 'PNL'


class BinanceTradeTypes(Enum):
    PERPETUAL = 'PERPETUAL'
    DELIVERY = 'DELIVERY'
