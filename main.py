import logging
from pytz import utc

from apscheduler.schedulers.blocking import BlockingScheduler
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker

from binance_leaderboard import BinanceLeaderboard
from leaderboard_enums import *
from utils import get_current_unix_ts
from config import TelegramCredential, MySQLLoginDetails
from tg_wrapper import TGBot
from models import BinanceLeaderboardTrades

logging.basicConfig(filename='project3.log', encoding='utf-8',
                    level=logging.INFO, format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')

tg_bot = TGBot(token=TelegramCredential.token_id, channel_id=TelegramCredential.channel_id)
bld = BinanceLeaderboard()
ref_uid = 'FB23E1A8B7E2944FAAEC6219BBDF8243'
sched = BlockingScheduler(timezone=utc)

prev_pos = []


def check_change_position(uid=ref_uid):
    global prev_pos
    print(prev_pos)

    new_pos = []
    change_in_pos = []
    current_positions = bld.get_positions(encrypted_uid=uid, trade_type=BinanceTradeTypes.PERPETUAL)
    pos_returns = current_positions.get('otherPositionRetList')
    print(pos_returns)

    if len(pos_returns) == 1:
        new_pos.append({
                'symbol': pos_returns[0]['symbol'],
                'amount': pos_returns[0]['amount'],
                'update_timestamp': pos_returns[0]['updateTimeStamp']/1000
            })
    elif len(pos_returns) > 1:
        for pr in pos_returns:
            new_pos.append({
                'symbol': pr['symbol'],
                'amount': pr['amount'],
                'update_timestamp': pr['updateTimeStamp']/1000
            })

    if prev_pos is None:
        prev_pos = new_pos
        return None
    else:
        if len(new_pos) == 0:
            if len(prev_pos) == 0:
                return None
            else:
                current_unixts = get_current_unix_ts()

                if len(prev_pos) == 1:
                    change_in_pos.append({
                            'symbol': prev_pos[0]['symbol'],
                            'amount': - prev_pos[0]['amount'],
                            'update_timestamp': current_unixts
                        })
                else:
                    for pp in prev_pos:
                        change_in_pos.append({
                            'symbol': pp['symbol'],
                            'amount': - pp['amount'],
                            'update_timestamp': current_unixts
                        })
        else:
            if len(prev_pos) == 0:
                change_in_pos = new_pos
            else:
                change_in_pos = None
                # Left as a Homework.

    prev_pos = new_pos
    return change_in_pos


@sched.scheduled_job('interval', minutes=1)
def bn_leaderboard_pipeline():
    try:
        engine = db.create_engine(MySQLLoginDetails.db_url, echo=True, pool_recycle=28800)
        connection = engine.connect()
        Session = sessionmaker(bind=engine)
        session = Session()
        table_data = []

        change_in_pos = check_change_position(uid=ref_uid)
        print(change_in_pos)
        if change_in_pos is not None:
            receive_unixts = get_current_unix_ts()

            if len(change_in_pos) == 1:
                table_data.append({
                    'uid': ref_uid,
                    'symbol': change_in_pos[0]['symbol'],
                    'amount': change_in_pos[0]['amount'],
                    'update_timestamp': change_in_pos[0]['update_timestamp'],
                    'receive_unixts': receive_unixts
                })

                tg_bot.send_message(str({
                    'uid': ref_uid,
                    'symbol': change_in_pos[0]['symbol'],
                    'amount': change_in_pos[0]['amount'],
                    'update_timestamp': change_in_pos[0]['update_timestamp'],
                    'receive_unixts': receive_unixts
                }))

            else:
                for cp in change_in_pos:
                    table_data.append({
                        'uid': ref_uid,
                        'symbol': cp['symbol'],
                        'amount': cp['amount'],
                        'update_timestamp': cp['update_timestamp'],
                        'receive_unixts': receive_unixts
                    })

                    tg_bot.send_message(str({
                        'uid': ref_uid,
                        'symbol': cp['symbol'],
                        'amount': cp['amount'],
                        'update_timestamp': cp['update_timestamp'],
                        'receive_unixts': receive_unixts
                    }))

            insert_query = db.insert(BinanceLeaderboardTrades)
            result_proxy = connection.execute(insert_query, table_data)

            logging.info('Committing Insertion')
            connection.commit()
            logging.info('Committed Insertion')

            connection.close()
            logging.info('Closed Connection.')

    except Exception as e:
        logging.error(e.__str__())
        tg_bot.send_message(f'Error: {e.__str__()}')


if __name__ == '__main__':
    try:
        logging.info('Scheduler Starts.')
        tg_bot.send_message('Scheduler Starts.')
        sched.start()
    except (KeyboardInterrupt, SystemExit):
        pass



