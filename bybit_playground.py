from pybit.unified_trading import HTTP

from config import BybitCredentials
from execution_algo import custom_chasing_limit_order

session = HTTP(
    testnet=True,
    api_key=BybitCredentials.API_KEY,
    api_secret=BybitCredentials.API_SECRET,
)

custom = custom_chasing_limit_order(
    http_session=session,
    orderbook_depth=5,
    max_refresh_tries=10,
    refresh_order_seconds=1,
    category="linear",
    symbol="BTCUSDT",
    side="Buy",
    qty='0.05'
)


ob = session.get_orderbook(category="linear", symbol="BTCUSDT")
best_bid_px = ob['result']['b'][10][0]
best_ask_px = ob['result']['a'][0][0]

wb = session.get_wallet_balance(accountType="UNIFIED")

po = session.place_order(
    category="linear",
    symbol="BTCUSDT",
    side="Sell",
    orderType="Limit",
    price=best_bid_px,
    qty=0.001,
)


order_id = po['result']['orderId']

open_orders = session.get_open_orders(category="linear", symbol="BTCUSDT", orderId=order_id)
order_status = open_orders['result']['list'][0]['orderStatus']

cancel_order = session.cancel_order(category="linear", symbol="BTCUSDT", orderId=order_id)

info = session.get_instruments_info(category="linear", symbol="BTCUSDT")
lot_size_filter = info['result']['list'][0]['lotSizeFilter']
